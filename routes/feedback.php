<?php

$prefix = config('lipton.admin_prefix');

Route::group(['as' => 'lipton.', 'prefix' => $prefix, 'middleware' => ['web']], function () {

    $namespacePrefix = '\\KevinKao\\LiptonFeedback\\Http\\Controllers';
    Route::group(['as' => 'feedback.', 'middleware' => ['admin.user']], function () use ($namespacePrefix) {
        Route::get('/feedback', "{$namespacePrefix}\FeedbackController@index")->name('index');
        Route::post('/feedback/datatable', "{$namespacePrefix}\FeedbackController@datatable")->name('datatable');
        Route::put('/feedback/{id}/status', "{$namespacePrefix}\FeedbackController@changeStatus")->name('status');
        Route::delete('/feedback/{id}', "{$namespacePrefix}\FeedbackController@destroy")->name('destroy');
    });

});

