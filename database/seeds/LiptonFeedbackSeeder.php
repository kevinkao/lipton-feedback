<?php

use Illuminate\Database\Seeder;

class LiptonFeedbackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [ 'key' => 'feedback_delete', 'display_name' => 'lipton-feedback::common.permission.feedback_delete' ],
            [ 'key' => 'feedback_edit', 'display_name' => 'lipton-feedback::common.permission.feedback_edit' ],
            [ 'key' => 'feedback_browse', 'display_name' => 'lipton-feedback::common.permission.feedback_browse' ],
        ]);
    }
}