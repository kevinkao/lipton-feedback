<?php

return [
    'feedback' => '投诉建议',
    'nav' => [
        'index' => '投诉建议'
    ],

    'permission' => [
        'feedback_delete' => '删除投诉建议',
        'feedback_edit' => '编辑投诉建议',
        'feedback_browse' => '查看投诉建议',
    ],

    'table' => [
        'title'        => '标题',
        'name'         => '姓名',
        'qq'           => 'QQ',
        'email'        => '郵箱地址',
        'phone_number' => '联络电话',
        'content'      => '投诉内容',
        'status'       => '状态',
        'created_at'   => '投诉时间',
        'action'       => '操作',
    ],
    'status' => [
        'unfinished' => '未处理',
        'finished' => '已处理'
    ]
];