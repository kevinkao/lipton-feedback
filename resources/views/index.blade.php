
@extends('lipton::layout')

@push('pre-styles')
<link rel="stylesheet" href="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.css') }}">
<style>
    table .action {
        margin-right: .3rem;
    }
    .toggle-wrap {
        padding-top: .375rem;
        padding-bottom: .375rem;
    }
</style>
@endpush

@section('page_toolbar')
<div class="toolbar">
    <button class="btn btn-success" id="fresh-table">{{ __('lipton::user.toolbar.fresh') }}</button>
</div>
@endsection

@section('page_breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route(config('lipton.default_route')) }}">{{ __('lipton::common.nav.home') }}</a></li>
        <li class="breadcrumb-item active">{{ __('lipton-feedback::common.nav.index') }}</li>
    </ol>
@endsection

@section('container')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                        <th>{{ __('lipton-feedback::common.table.name') }}</th>
                        <th>{{ __('lipton-feedback::common.table.email') }}</th>
                        <th>{{ __('lipton-feedback::common.table.phone_number') }}</th>
                        <th>{{ __('lipton-feedback::common.table.created_at') }}</th>
                        <th>{{ __('lipton-feedback::common.table.status') }}</th>
                        <th>{{ __('lipton-feedback::common.table.action') }}</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade feedback-modal" id="feedback-modal" tabindex="-1" role="dialog" aria-labelledby="create-form-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="overlay hidden">
                <i class="fas fa-2x fa-sync-alt"></i>
            </div>
            <div class="modal-header">
                <h5 class="modal-title">{{ __('lipton-feedback::common.table.content') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">
                            {{ __('lipton-feedback::common.table.name') }}
                        </label>
                        <div class="col-sm-10">
                          <input type="text" readonly class="form-control-plaintext"name="name" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">
                            {{ __('lipton-feedback::common.table.email') }}
                        </label>
                        <div class="col-sm-10">
                          <input name="email" type="text" readonly class="form-control-plaintext" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">
                            {{ __('lipton-feedback::common.table.phone_number') }}
                        </label>
                        <div class="col-sm-10">
                            <input name="phone_number" type="text" readonly class="form-control-plaintext" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">
                            {{ __('lipton-feedback::common.table.status') }}
                        </label>
                        <div class="col-sm-10">
                            <div class="toggle-wrap">
                                <input type="checkbox" class="status-toggle"
                                data-on="{{ __('lipton-feedback::common.status.finished') }}"
                                data-off="{{ __('lipton-feedback::common.status.unfinished') }}" data-width="100">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">
                            {{ __('lipton-feedback::common.table.content') }}
                        </label>
                        <div class="col-sm-10">
                          <textarea name="content" class="form-control-plaintext" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('lipton::role.modal.close') }}</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ Lipton::assets('/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ Lipton::assets('/bootstrap4-toggle/bootstrap4-toggle.min.js') }}"></script>
@datatable_locale()
<script>
    $('#fresh-table').on('click', function() {
        $('#datatable').DataTable().ajax.reload(null, false);
    });
    $('.status-toggle').bootstrapToggle({ size: 'sm'});
    $('#datatable').DataTable({
        "autoWidth": false,
        "serverSide": true,
        "processing": true,
        "stateSave": true,
        "order": [4, "asc"],
        "ajax": {
            url: '{{ route('lipton.feedback.datatable') }}',
            type: 'post'
        },
        "createdRow": function(row, data, dataIndex) {
            $(row).data(data);
        },
        "columns": [
            { "data": "name", "orderable": false },
            { "data": "email", "orderable": false },
            { "data": "phone_number", "orderable": false },
            { "data": "created_at" },
            { "data": "statusText" },
            { "data": "actions", "orderable": false, "width": "15%", "className": "text-center", render: function(data, type, row) {
                var $browse = $('<button/>', {
                        "data-target": "#feedback-modal",
                        "data-toggle": "modal"
                    })
                    .addClass('btn bg-gradient-info btn-sm action browse')
                    .html('<i class="fas fa-search"></i>');

                if (!row.canEdit) {
                    $browse.prop('disabled', true).addClass('disabled');
                }

                var $delete = $('<button/>', {'data-id': row.id})
                    .addClass('btn bg-gradient-danger btn-sm action delete')
                    .html('<i class="fas fa-trash"></i>')
                if (!row.canDelete) {
                    $delete.prop('disabled', true).addClass('disabled');
                }
                return $browse.prop('outerHTML')+$delete.prop('outerHTML');
            } }
        ]
    })
    .on('click', '.delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: '{{ __('lipton::common.actions.delete_confirmed') }}',
            text: "{{ __('lipton::common.actions.delete_hint') }}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{ __('lipton::common.actions.delete_confirm_text') }}',
            cancelButtonText: '{{ __('lipton::common.actions.delete_cancel_text') }}'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ route('lipton.feedback.destroy', ':id') }}'.replace(':id', id),
                    type: 'delete'
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                });
            }
        });
    });

    $('#feedback-modal')
        .on('show.bs.modal', function(e) {
            var $button = $(e.relatedTarget);
            var data = $button.parents('tr').data();

            $(this).data(data);

            $('input[name="name"]', this).val(data.name);
            $('input[name="email"]', this).val(data.email);
            $('input[name="phone_number"]', this).val(data.phone_number);
            $('.status-toggle', this).data('orignStatus', data.status);
            if (data.status == 0) {
                $('.status-toggle', this).bootstrapToggle('off');
            } else {
                $('.status-toggle', this).bootstrapToggle('on');
            }
            $('textarea[name="content"]', this).val(data.content);
        })
        .on('hidden.bs.modal', function(e) {
            $('input', this).val('');
            $('textarea', this).val('');
            var data = $(this).data();

            var status = $('.status-toggle', this).prop('checked') ? 1 : 0;
            var orignStatus = $('.status-toggle', this).data('orignStatus');
            if (status != orignStatus) {
                $.ajax({
                    url: '{{ route('lipton.feedback.status', ':id') }}'.replace(':id', data.id),
                    type: 'put',
                    headers: { 'Content-type': 'application/json' },
                    data: JSON.stringify({
                        'status': status
                    })
                }).done(function(resp) {
                    if (resp.code > 0) {
                        Toast.fire({ type: 'error', title: resp.message });
                        return;
                    }
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Toast.fire({ type: 'success', title: resp.message });
                });
            }
        });
</script>
@endpush