<?php

namespace KevinKao\LiptonFeedback\Http\Controllers;

use Illuminate\Http\Request;

use KevinKao\Lipton\Errors;
use KevinKao\Lipton\Http\Controllers\Controller;
use KevinKao\LiptonFeedback\Models\Feedback;
use Log;


class FeedbackController extends Controller
{
    const STATUS_UNFINISHED = 0;
    const STATUS_FINISHED = 1;

    public function index()
    {
        $this->authorize('browse', 'feedback');

        return view('lipton-feedback::index');
    }

    public function datatable(Request $request)
    {
        $this->authorize('browse', 'feedback');

        $columns = ['name', 'email', 'phone_number', 'created_at', 'status', 'actions'];

        $totalRows = Feedback::count();
        $totalFiltered = $totalRows;
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $query = Feedback::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir);

            $rows = $query->get();
        } else {
            $search = $request->input('search.value');

            $rows = Feedback::where('phone_number', 'LIKE', "{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = $rows->count();
        }

        $userCanEdit = auth()->user()->can('edit', 'feedback');
        $userCanDelete = auth()->user()->can('delete', 'feedback');
        foreach ($rows as &$row) {
            $row['canEdit'] = $userCanEdit;
            $row['canDelete'] = $userCanDelete;
            $row['statusText'] = $row['status'] == self::STATUS_UNFINISHED ? 
                                    __('lipton-feedback::common.status.unfinished') :
                                    __('lipton-feedback::common.status.finished');
            $row['actions'] = '';
        }

        return response()->json([
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalRows),
            "recordsFiltered" => intval($totalFiltered),
            'data' => $rows
        ]);
    }

    public function changeStatus(Request $request, $id)
    {
        try {
            $this->authorize('edit', 'feedback');

            $validated = $request->validate([
                'status' => 'required|numeric'
            ]);

            $feedback = Feedback::findOrFail($id);
            $feedback->status = $request->input('status');
            $feedback->save();

            return response()->json([
                'code' => 0,
                'message' => __("lipton::common.actions.success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => __("lipton::common.actions.fail", ['message' => $e->getMessage()])
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $this->authorize('delete', 'feedback');

            $feedback = Feedback::findOrFail($id);
            $feedback->delete();
            return response()->json([
                'code' => 0,
                'message' => __("lipton::common.actions.success"),
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'code' => $e->getCode() == 0 ? Errors::UNKNOWN_ERROR : intval($e->getCode()),
                'message' => __("lipton::common.actions.fail", ['message' => $e->getMessage()])
            ]);
        }
    }
}