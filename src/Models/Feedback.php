<?php

namespace KevinKao\LiptonFeedback\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';
    protected $fillable = ['name', 'title', 'email', 'qq', 'phone_number', 'content'];
}