<?php

namespace KevinKao\LiptonFeedback\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class FeedbackProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $tmp = config('lipton.nav');
        array_push(
            $tmp['lipton::common.nav.system'],
            \KevinKao\LiptonFeedback\NavItems\FeedbackNavItem::class
        );
        config(['lipton.nav' => $tmp]);

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'lipton-feedback');
        $this->loadRoutesFrom(__DIR__.'/../../routes/feedback.php');
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../database/migrations'));
        $this->loadTranslationsFrom(realpath(__DIR__.'/../../resources/lang'), 'lipton-feedback');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->runningInConsole()) {
            $dir = dirname(__DIR__);

            $this->publishes([
                "{$dir}/../database/seeds/" => database_path('seeds')
            ]);
        }
    }
}