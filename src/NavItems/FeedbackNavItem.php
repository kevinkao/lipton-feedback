<?php

namespace KevinKao\LiptonFeedback\NavItems;

use KevinKao\Lipton\NavItems\NavItem;
use Gate;

/**
* 系統設置
*/
class FeedbackNavItem extends NavItem
{
    public $title = 'lipton-feedback::common.feedback';
    public $iconClass = 'fa-comment';
    public $hasTreeView = false;
    protected $childItemsClass = [
        // 
    ];

    public function getLink($fullUrl = true)
    {
        return route('lipton.feedback.index', [], $fullUrl);
    }

    public function isVisible()
    {
        return Gate::allows('browse', 'feedback');
    }
}
